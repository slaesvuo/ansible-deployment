---
title: Otaniemen lukion fablab
---

# Otaniemen lukion fablab

## Telegram

[Yhdistä Telegram ryhmä nyt](https://t.me/joinchat/WYaf48cddcMzZjU8)

## Säännöt

1. Vie kaikki roskat roskiin
2. Älä käytä useaa tulostinta samaan aikaan
3. Keskeytä tulostus joka epäonnistuu

## Portaali

[Avaa RiP portaali](/portal)

Portaalin salanasanan saat [ylläpidosta](#ylläpidon-yhteystiedot)

## Configurations

- [Ultimaker 2+](https://gitlab.com/otafablab/prusaslicer-config/-/raw/main/ultimaker2+.ini?inline=false)
- [Prenta Duo XL](https://gitlab.com/otafablab/prusaslicer-config/-/raw/main/prenta_duo_xl.ini?inline=false)

## 3D-mallintaminen ja suuunnittelu

- [Osa 1](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-1/)
- [Osa 2](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-2/)
- [Osa 3](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-3/)
- [Osa 4](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-4/)
- [Osa 5](https://www.lukemaverkosto.fi/materiaali/3d-mallintaminen-ja-tulostaminen-lukiokurssi-osa-5/)

## Ylläpidon yhteystiedot

- Niklas Halonen, Fablab toimitusjohtaja
  - <https://t.me/niklashh>
- Matias Zwinger, Fablab varapuheenjohtaja ja laboratoriomanageri
  - <https://t.me/matias_z>
- Matti Heikkinen, Matematiikan ja fysiikan opettaja
  - <https://t.me/Heikkinen>
