use std::env::args;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::{Read, Write};
use std::path::PathBuf;

fn main() {
    let mut toml = String::with_capacity(8192);
    let mut file = OpenOptions::new()
        .read(true)
        .open(args().nth(1).unwrap())
        .unwrap();

    file.read_to_string(&mut toml).unwrap();

    let mut manifest = cargo_toml::Manifest::from_str(&toml).unwrap();

    let mut package = manifest.package.take().unwrap();

    let mut split = package.version.split('.');
    let mut major: u32 = split.next().unwrap().parse().unwrap();
    let mut minor: u32 = split.next().unwrap().parse().unwrap();
    let mut patch: u32 = split.next().unwrap().parse().unwrap();

    patch += 1;
    if patch == 9 {
        minor += 1;
        patch = 0;
    }
    if minor == 9 {
        major += 1;
        patch = 0;
    }
    let new_version = format!("{}.{}.{}", major, minor, patch);

    println!("Bump {} => {}", package.version, &new_version);

    package.version = new_version;

    manifest.package = Some(package);

    let contents = toml::to_string(&manifest).unwrap();

    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .truncate(true)
        .open(args().nth(1).unwrap())
        .unwrap();
    file.write_all(contents.as_ref()).unwrap();
}
